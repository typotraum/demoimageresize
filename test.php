<?php
/**

 */

include "DemoImageResize.php";

use DemoImageResize\DemoImageResize;

$original_width = 500;
$original_height = 400;
$target_width= 180;
$target_height=250;

echo 'original '.$original_width.' x '.$original_height."\n";
echo 'target '.$target_width.' x '.$target_height."\n";


$test = new DemoImageResize($original_width,$original_height);
$test->setUpscaleSmaller(0);
$test->setTargetDimensions($target_width,$target_height);

echo 'scale :'.$test->getScalingFactor()."\n";
echo 'new dimensions:'.($original_width * $test->getScalingFactor()).' x '.($original_height * $test->getScalingFactor())."\n";
