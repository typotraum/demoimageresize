<?php
/**
 * DemoImageResize Demo Class
 * Created for demo reasons
 * User: typotraum
 */

namespace DemoImageResize;

/**
 * Class DemoImageResize
 * @package DemoImageResize
 */

class DemoImageResize
{

    /**
     * @var int
     */
    protected $original_width;
    /**
     * @var int
     */
    protected $original_height;

    /**
     * @var float
     */
    protected $original_ratio;

    /**
     * @var int
     */
    protected $target_width = 1;
    /**
     * @var int
     */
    protected $target_height = 1;
    /**
     * @var float
     */
    protected $scaling_factor;

    /**
     * @var boolean
     */
    protected $original_smaller_than_target = false;

    /**
     * @var boolean
     */
    protected $upscale_smaller = false;


    /**
     * DemoImageResize constructor.
     * @param int $original_width
     * @param int $original_height
     */
    public function __construct(int $original_width, int $original_height)
    {
        $this->original_width = $original_width;
        $this->original_height = $original_height;
        $this->calculate();
    }

    private function calculate()
    {

        $this->original_ratio = $this->original_width / $this->original_height;

        if ($this->original_width < $this->target_width && $this->original_height < $this->target_height)
            $this->original_smaller_than_target = true;


    }

    /**
     * @param int $width
     * @return void
     */
    public function setOriginalWidth(int $original_width)
    {

        $this->original_width = $original_width;
        $this->calculate();
        return;
    }

    /**
     * @param int $height
     * @return void
     */
    public function setOriginalHeight(int $original_height)
    {
        $this->original_height = $original_height;
        $this->calculate();
        return;
    }


    /**
     * @param int $height
     * @return void
     */
    public function setOriginalDimensions(int $original_width, int $original_height)
    {
        $this->original_height = $original_width;
        $this->original_height = $original_height;
        $this->calculate();
        return;
    }

    /**
     * @param int $height
     * @return void
     */
    public function setTargetDimensions(int $target_width, int $target_height)
    {
        $this->target_width = $target_width;
        $this->target_height = $target_height;
        $this->calculate();
        return;
    }

    /**
     * @param bool $upscale_smaller
     * @return void
     */

    public function setUpscaleSmaller(bool $upscale_smaller)
    {
        $this->upscale_smaller = $upscale_smaller;
        return;
    }


    private function calculateRatio()
    {

        if ($this->original_smaller_than_target && !$this->upscale_smaller
        ) {
            $ratio = 1;
            return $ratio;
        }


        $ratio = $this->target_width / $this->original_width;
        $calculated_height = $this->original_height * $ratio;
        if ($calculated_height <= $this->target_height) {
            //    fits
            return $ratio;
        } else {
            //  doesnt'fit
            $ratio = $this->target_height / $this->original_height;
            return $ratio;
        }


    }


    /**
     * @return float
     */
    public function getScalingFactor()
    {

        // var_dump($this);
        $this->scaling_factor = $this->calculateRatio();


        return $this->scaling_factor;

    }


}